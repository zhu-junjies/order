package main.service;

import main.entity.Dishitem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Dishitem)表服务接口
 *
 * @author makejava
 * @since 2021-06-06 19:55:54
 */
public interface DishitemService {

    /**
     * 通过ID查询单条数据
     *
     * @param dishId 主键
     * @return 实例对象
     */
    List<Dishitem> queryByBusId(String BusIdd);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Dishitem> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param dishitem 实例对象
     * @return 实例对象
     */
    Dishitem insert(Dishitem dishitem);

    /**
     * 修改数据
     *
     * @param dishitem 实例对象
     * @return 实例对象
     */
    Dishitem update(Dishitem dishitem);

    /**
     * 通过主键删除数据
     *
     * @param dishname 主键
     * @return 是否成功
     */
    boolean deleteById(String dishId);

    Dishitem quetyByDishID(String dishId);

    Dishitem queryByDishName(String dishName);

    Dishitem queryByDishNameAndBusId( String dishName, String busId);

}
