package main.service;

import main.entity.Gender;

import java.util.List;

/**
 * (Gender)表服务接口
 *
 * @author makejava
 * @since 2021-06-08 14:24:55
 */
public interface GenderService {

    /**
     * 通过ID查询单条数据
     *
     * @param username 主键
     * @return 实例对象
     */
    Gender queryById(String username);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Gender> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param gender 实例对象
     * @return 实例对象
     */
    Gender insert(Gender gender);

    /**
     * 修改数据
     *
     * @param gender 实例对象
     * @return 实例对象
     */
    Gender update(Gender gender);

    /**
     * 通过主键删除数据
     *
     * @param username 主键
     * @return 是否成功
     */
    boolean deleteById(String username);

}
