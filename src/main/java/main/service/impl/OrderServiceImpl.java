package main.service.impl;

import main.entity.Message;
import main.entity.Order;
import main.dao.OrderDao;
import main.service.OrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Order)表服务实现类
 *
 * @author makejava
 * @since 2021-06-05 09:53:30
 */
@Service("orderService")
public class OrderServiceImpl implements OrderService {
    @Resource
    private OrderDao orderDao;

    /**
     * 通过ID查询单条数据
     *
     * @param orderid 主键
     * @return 实例对象
     */
    @Override
    public Order queryById(String orderid) {
        return this.orderDao.queryById(orderid);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Order> queryAllByLimit(int offset, int limit) {
        return this.orderDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param order 实例对象
     * @return 实例对象
     */
    @Override
    public Order insert(Order order) {
        this.orderDao.insert(order);
        return order;
    }

    /**
     * 修改数据
     *
     * @param order 实例对象
     * @return 实例对象
     */
    @Override
    public Order update(Order order) {
        this.orderDao.update(order);
        return this.queryById(order.getOrderid());
    }

    /**
     * 通过主键删除数据
     *
     * @param orderid 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String orderid) {
        return this.orderDao.deleteById(orderid) > 0;
    }

    @Override
    public List<Order> queryByBusId(String BusId) {
        return orderDao.queryByBusId(BusId);
    }

    @Override
    public List<Order> queryByConsumer(String consumer) {
        return orderDao.queryByConsumer(consumer);
    }

    @Override
    public void updateStatusByOrderid(String dishId) {
        orderDao.updateStatusByOrderid(dishId);
    }

    @Override
    public List<String>queryBusidByConsumer(String consumer){
        return orderDao.queryBusidByConsumer(consumer);
    }



}
