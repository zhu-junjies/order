package main.service.impl;

import main.entity.Bus;
import main.dao.BusDao;
import main.service.BusService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Bus)表服务实现类
 *
 * @author makejava
 * @since 2021-06-05 13:57:56
 */
@Service("busService")
public class BusServiceImpl implements BusService {
    @Resource
    private BusDao busDao;

    /**
     * 通过ID查询单条数据
     *
     * @param businessname 主键
     * @return 实例对象
     */
    @Override
    public Bus queryByBusinessName(String businessname) {
        return this.busDao.queryById(businessname);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Bus> queryAllByLimit(int offset, int limit) {
        return this.busDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param bus 实例对象
     * @return 实例对象
     */
    @Override
    public Bus insert(Bus bus) {
        this.busDao.insert(bus);
        return bus;
    }

    /**
     * 修改数据
     *
     * @param bus 实例对象
     * @return 实例对象
     */
    @Override
    public Bus update(Bus bus) {
        this.busDao.update(bus);
        return this.queryByBusinessName(bus.getBusinessname());
    }

    /**
     * 通过主键删除数据
     *
     * @param businessname 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String businessname) {
        return this.busDao.deleteById(businessname) > 0;
    }

    @Override
    public Integer getBusCount() {
       return busDao.queryCount();
    }

    @Override
    public Bus queryOneByUsername(String username) {
        return busDao.queryOneByUsername(username);
    }
}
