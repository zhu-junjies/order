package main.service.impl;

import main.entity.Dishitem;
import main.dao.DishitemDao;
import main.service.DishitemService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Dishitem)表服务实现类
 *
 * @author makejava
 * @since 2021-06-06 19:55:55
 */
@Service("dishitemService")
public class DishitemServiceImpl implements DishitemService {
    @Resource
    private DishitemDao dishitemDao;


    @Override
    public List<Dishitem> queryByBusId(String busId) {
        return this.dishitemDao.queryByBusId(busId);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Dishitem> queryAllByLimit(int offset, int limit) {
        return this.dishitemDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param dishitem 实例对象
     * @return 实例对象
     */
    @Override
    public Dishitem insert(Dishitem dishitem) {
        this.dishitemDao.insert(dishitem);
        return dishitem;
    }

    /**
     * 修改数据
     *
     * @param dishitem 实例对象
     * @return 实例对象
     */
    @Override
    public Dishitem update(Dishitem dishitem) {
        this.dishitemDao.update(dishitem);
        return this.quetyByDishID(dishitem.getDishid());
    }

    /**
     * 通过主键删除数据
     *
     * @param dishname 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String dishId) {
        return this.dishitemDao.deleteById(dishId) > 0;
    }

    @Override
    public Dishitem quetyByDishID(String dishId) {
        return null;
    }

    @Override
    public Dishitem queryByDishName(String dishName) {
        return this.dishitemDao.queryByDishName(dishName);
    }

    @Override
    public Dishitem queryByDishNameAndBusId(String dishName, String busId) {
        return this.dishitemDao.queryByDishNameAndBusId(dishName,busId);
    }
}
