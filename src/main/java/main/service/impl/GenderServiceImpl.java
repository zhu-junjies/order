package main.service.impl;

import main.entity.Gender;
import main.dao.GenderDao;
import main.service.GenderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Gender)表服务实现类
 *
 * @author makejava
 * @since 2021-06-08 14:24:55
 */
@Service("genderService")
public class GenderServiceImpl implements GenderService {
    @Resource
    private GenderDao genderDao;

    /**
     * 通过ID查询单条数据
     *
     * @param username 主键
     * @return 实例对象
     */
    @Override
    public Gender queryById(String username) {
        return this.genderDao.queryById(username);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Gender> queryAllByLimit(int offset, int limit) {
        return this.genderDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param gender 实例对象
     * @return 实例对象
     */
    @Override
    public Gender insert(Gender gender) {
        this.genderDao.insert(gender);
        return gender;
    }

    /**
     * 修改数据
     *
     * @param gender 实例对象
     * @return 实例对象
     */
    @Override
    public Gender update(Gender gender) {
        this.genderDao.update(gender);
        return this.queryById(gender.getUsername());
    }

    /**
     * 通过主键删除数据
     *
     * @param username 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String username) {
        return this.genderDao.deleteById(username) > 0;
    }
}
