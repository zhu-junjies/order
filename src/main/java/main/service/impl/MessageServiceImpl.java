package main.service.impl;

import main.entity.Message;
import main.dao.MessageDao;
import main.service.MessageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Message)表服务实现类
 *
 * @author makejava
 * @since 2021-06-09 08:33:14
 */
@Service(value = "messageService")
public class MessageServiceImpl implements MessageService {
    @Resource
    private MessageDao messageDao;

    /**
     * 通过ID查询单条数据
     *
     * @param 主键
     * @return 实例对象
     */

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Message> queryAllByLimit(int offset, int limit) {
        return this.messageDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param message 实例对象
     * @return 实例对象
     */
    @Override
    public Message insert(Message message) {
        this.messageDao.insert(message);
        return message;
    }

    @Override
    public List<Message> queryByUsername(String username, String toUsername) {
        return messageDao.queryByUsername(username,toUsername);
    }

    /**
     * 修改数据
     *
     * @param message 实例对象
     * @return 实例对象
     */

    /**
     * 通过主键删除数据
     *
     * @param 主键
     * @return 是否成功
     */

}
