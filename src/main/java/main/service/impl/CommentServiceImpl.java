package main.service.impl;

import main.entity.Comment;
import main.dao.CommentDao;
import main.service.CommentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Comment)表服务实现类
 *
 * @author makejava
 * @since 2021-06-07 10:10:23
 */
@Service("commentService")
public class CommentServiceImpl implements CommentService {
    @Resource
    private CommentDao commentDao;

    /**
     * 通过ID查询单条数据
     *
     * @param
     * @return 实例对象
     */

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Comment> queryAllByLimit(int offset, int limit) {
        return this.commentDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param comment 实例对象
     * @return 实例对象
     */
    @Override
    public Comment insert(Comment comment) {
        this.commentDao.insert(comment);
        return comment;
    }

    @Override
    public void update(Comment comment) {
        commentDao.update(comment);
    }

    @Override
    public Double queryAvgRate(String BusId) {
        return commentDao.queryAvgRate(BusId);
    }

    @Override
    public List<Comment> queryByBusid(String Busid, int pageStart, int limit) {
        return commentDao.queryByBusid(Busid,pageStart,limit);
    }

    @Override
    public int queryCount(String Busid) {
        return commentDao.queryCount(Busid);
    }

    @Override
    public void updateReply(String busId, String replyMessage,String username) {
        commentDao.updateReply(busId, replyMessage,username);
    }

    @Override
    public Comment queryByBusIdAndUsername(String username, String busId) {
        return commentDao.queryByBusIdAndUsername(username, busId);
    }
}
