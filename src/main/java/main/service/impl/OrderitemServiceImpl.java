package main.service.impl;

import main.entity.Orderitem;
import main.dao.OrderitemDao;
import main.service.OrderitemService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Orderitem)表服务实现类
 *
 * @author makejava
 * @since 2021-06-05 09:53:20
 */
@Service("orderitemService")
public class OrderitemServiceImpl implements OrderitemService {
    @Resource
    private OrderitemDao orderitemDao;

    /**
     * 通过ID查询单条数据
     *
     * @param dishname 主键
     * @return 实例对象
     */
    @Override
    public Orderitem queryById(String dishname) {
        return this.orderitemDao.queryById(dishname);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Orderitem> queryAllByLimit(int offset, int limit) {
        return this.orderitemDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param orderitem 实例对象
     * @return 实例对象
     */
    @Override
    public Orderitem insert(Orderitem orderitem) {
        this.orderitemDao.insert(orderitem);
        return orderitem;
    }

    /**
     * 修改数据
     *
     * @param orderitem 实例对象
     * @return 实例对象
     */
    @Override
    public Orderitem update(Orderitem orderitem) {
        this.orderitemDao.update(orderitem);
        return this.queryById(orderitem.getDishName());
    }

    /**
     * 通过主键删除数据
     *
     * @param dishname 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String dishname) {
        return this.orderitemDao.deleteById(dishname) > 0;
    }

    @Override
    public int insertBatch(List<Orderitem> entities) {
        return orderitemDao.insertBatch(entities);
    }
}
