package main.service;

import main.entity.Orderitem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Orderitem)表服务接口
 *
 * @author makejava
 * @since 2021-06-05 09:53:20
 */
public interface OrderitemService {

    /**
     * 通过ID查询单条数据
     *
     * @param dishname 主键
     * @return 实例对象
     */
    Orderitem queryById(String dishname);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Orderitem> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param orderitem 实例对象
     * @return 实例对象
     */
    Orderitem insert(Orderitem orderitem);

    /**
     * 修改数据
     *
     * @param orderitem 实例对象
     * @return 实例对象
     */
    Orderitem update(Orderitem orderitem);

    /**
     * 通过主键删除数据
     *
     * @param dishname 主键
     * @return 是否成功
     */
    boolean deleteById(String dishname);

    int insertBatch(@Param("entities") List<Orderitem> entities);

}
