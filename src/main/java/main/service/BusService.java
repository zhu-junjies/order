package main.service;

import main.entity.Bus;

import java.util.List;

/**
 * (Bus)表服务接口
 *
 * @author makejava
 * @since 2021-06-05 13:57:56
 */
public interface BusService {

    /**
     * 通过ID查询单条数据
     *
     * @param businessname 主键
     * @return 实例对象
     */
    Bus queryByBusinessName(String businessname);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Bus> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param bus 实例对象
     * @return 实例对象
     */
    Bus insert(Bus bus);

    /**
     * 修改数据
     *
     * @param bus 实例对象
     * @return 实例对象
     */
    Bus update(Bus bus);

    /**
     * 通过主键删除数据
     *
     * @param businessname 主键
     * @return 是否成功
     */
    boolean deleteById(String businessname);

    Integer getBusCount();

    Bus queryOneByUsername(String username);

}
