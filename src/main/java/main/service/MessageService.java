package main.service;

import main.entity.Message;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Message)表服务接口
 *
 * @author makejava
 * @since 2021-06-09 08:33:14
 */
public interface MessageService {


    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Message> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param message 实例对象
     * @return 实例对象
     */
    Message insert(Message message);


    List<Message> queryByUsername(@Param("username") String username, @Param("toUsername") String toUsername);
}
