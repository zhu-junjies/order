package main.service;

import main.entity.Comment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Comment)表服务接口
 *
 * @author makejava
 * @since 2021-06-07 10:10:23
 */
public interface CommentService {

    /**
     * 通过ID查询单条数据
     *
     * @param 主键
     * @return 实例对象
     */

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Comment> queryAllByLimit(int offset, int limit);


    /**
     * 新增数据
     *
     * @param comment 实例对象
     * @return 实例对象
     */
    Comment insert(Comment comment);

    /**
     * 修改数据
     *
     * @param comment 实例对象
     * @return 实例对象
     */
    void update(Comment comment);



    Double queryAvgRate(String BusId);

    List<Comment> queryByBusid( String Busid, int pageStart, int limit);

    int queryCount(String Busid);

    void updateReply(String busId,String replyMessage,String username);

    Comment queryByBusIdAndUsername(String username,String busId);

}
