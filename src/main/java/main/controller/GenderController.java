package main.controller;

import main.entity.Gender;
import main.entity.Order;
import main.service.GenderService;
import main.service.OrderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.Date;

/**
 * (Gender)表控制层
 *
 * @author makejava
 * @since 2021-06-08 14:24:56
 */
@RestController
@RequestMapping("gender")
public class GenderController {
    /**
     * 服务对象
     */
    @Resource
    private GenderService genderService;

    @Resource
    private OrderService orderService;
    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/getGender")
    public Gender selectOne(@CookieValue("username") String username) {
        return this.genderService.queryById(username);
    }
    @RequestMapping("/updateGender")
    public void update(Gender gender,@CookieValue("username")String username)
    {
        if (gender.getDate()!=null)
        {
            Long aLong = Long.valueOf(gender.getDate());
            Date date = new Date(aLong);
            gender.setDate(date.toString());
            gender.setMoney(null);
        }
        gender.setUsername(username);
        genderService.update(gender);
    }
    @RequestMapping("/toPay")
    public String toPay(Double money,@CookieValue("username")String username,String orderId)
    {
        Gender gender = genderService.queryById(username);
        Double money1 = gender.getMoney();
        if (money1<money)
        {
            return "no";
        }
        else {
            money1=money1-money;
            gender.setMoney(money1);
            genderService.update(gender);
            Order order = new Order();
            order.setOrderid(orderId);
            order.setStatus("0");
            orderService.update(order);
            return "ok";
        }
    }
}
