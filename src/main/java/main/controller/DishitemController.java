package main.controller;

import main.entity.Dishitem;
import main.service.DishitemService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * (Dishitem)表控制层
 *
 * @author makejava
 * @since 2021-06-06 19:55:56
 */
@RestController
@RequestMapping("dishitem")
public class DishitemController {
    /**
     * 服务对象
     */
    @Resource
    private DishitemService dishitemService;

    @RequestMapping("/addDishItem")
    public String addDishItem(Dishitem dishitem)
    {
        if (dishitemService.queryByDishNameAndBusId(dishitem.getDishname(),dishitem.getBusid())==null);
        {
            Date date = new Date();
            long time = date.getTime();
            dishitem.setDishid(dishitem.getBusid()+time);
            dishitemService.insert(dishitem);
        }
        return null;
    }
    @RequestMapping("/getDishItem")
    public List<Dishitem> getDishItem(String busId)
    {
        return dishitemService.queryByBusId(busId);
    }

    @RequestMapping("/deleteDishItem")
    public String deleDishItem(String dishId)
    {
        dishitemService.deleteById(dishId);
        return "ok";
    }

    @RequestMapping("/updateDishItem")
    public String updateDishItem(Dishitem dishItem)
    {
        Dishitem dishitem = dishitemService.queryByDishNameAndBusId(dishItem.getDishname(), dishItem.getBusid());
        if (dishitem!=null)
        {if (!dishItem.getDishid().equals(dishitem.getDishid()))
        {
            return "too much";
        }
        }

        Dishitem update = dishitemService.update(dishItem);
        return "ok";
    }
}
