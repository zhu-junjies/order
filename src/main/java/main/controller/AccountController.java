package main.controller;

import main.entity.Account;
import main.entity.Gender;
import main.service.AccountService;
import main.service.BusService;
import main.service.GenderService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * (Account)表控制层
 *
 * @author makejava
 * @since 2021-06-03 20:35:41
 */
@RestController
@RequestMapping("/account")
public class AccountController {
    /**
     * 服务对象
     */
    @Resource
    private AccountService accountService;

    @Resource
    private GenderService genderService;

    @Resource
    private BusService busService;

    /**
     * 通过主键查询单条数据
     *
     * @param account 主键
     * @return 单条数据
     */
    @RequestMapping("/register")
    public Account register(Account account, HttpServletRequest request)
    {
        Gender gender = new Gender();
        Account account1 = accountService.queryById(account.getUsername());
        if (account1==null)
        {
            accountService.insert(account);
            gender.setUsername(account.getUsername());
            gender.setMoney((double) 0);
            genderService.insert(gender);
        }
        else {
            account.setUsername("There are already");
        }
        return account;
    }
    @RequestMapping("/login")
    public Account login(Account account)
    {
        String username = account.getUsername();
        Account account1 = accountService.queryById(username);
        System.out.println(username+"登录了");
        if (account1==null)
        {   account.setUsername("fail");
            return account;
        }
        else if (account1.getPassword().equals(account.getPassword()))
        {
            if (busService.queryOneByUsername(account.getUsername())!=null)
            {

                account.setUsername("yes");
            }
            else {
                account.setUsername("no");
            }
            System.out.println(username+"登录了");
            return account;
        }
        else { account.setUsername("fail");
            return account;}
    }
    @RequestMapping("/updatePassword")
    public String updatePassword(@CookieValue("username")String username,@RequestParam("password")String password,@RequestParam("newPassword") String newPassword){
        Account account = new Account();
        account.setUsername(username);
        account.setPassword(password);
        Account login = this.login(account);
        if (!login.getUsername().equals("fail"))
        {
            account.setPassword(newPassword);
            account.setUsername(username);
            accountService.update(account);
        return "ok";
        }
        else {return "no";}
    }
}
