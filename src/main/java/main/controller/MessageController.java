package main.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import main.entity.Message;
import main.service.MessageService;
import main.service.impl.MessageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

import javax.annotation.Resource;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.sql.Timestamp;
import java.util.*;

/**
 * (Message)表控制层
 *
 * @author makejava
 * @since 2021-06-09 08:33:14
 */
@Component
@ServerEndpoint("/message/{username}/{busId}")
public class MessageController {


    private static MessageService messageService;
    @Autowired
    public void setMessageService(MessageService messageService){
        MessageController.messageService=messageService;
    }
    private static Map<String,Session> sessionMap = new HashMap<String, Session>();

    @OnOpen
    public void onOpen(Session session, @PathParam("username") String username,@PathParam("busId")String busId)
    {
        System.out.println(username+"和"+busId+"进入了连接");
        List<Message> messages = messageService.queryByUsername(username, busId);
        Object s = new Gson().toJson(messages);
        System.out.println(s);
//        session.getAsyncRemote().sendText((String) s);
        sessionMap.put(username,session);
        System.out.println(session.getAsyncRemote());
    }
    @OnMessage
    public void onMessage(String message,Session session,@PathParam("username") String username) throws JsonProcessingException {
        System.out.println(sessionMap);
        ObjectMapper objectMapper = new ObjectMapper();
        Message message1 = objectMapper.readValue(message, Message.class);
        message1.setUsername(username);
        Session session2 = sessionMap.get(message1.getTousername());
        sessionMap.put(username,session);
        Date date = new Date();
        Timestamp tm = new Timestamp(date.getTime());
        message1.setTime(tm.toString());
        messageService.insert(message1);
        List<Message> messages = messageService.queryByUsername(username, message1.getTousername());
        session.getAsyncRemote().sendText(new Gson().toJson(message1));
        System.out.println(new Gson().toJson(message1));
        if (session2!=null)
        {
            session2.getAsyncRemote().sendText(new Gson().toJson(message1));
        }
    }
    @OnClose
    public void onclose(@PathParam("username") String username){
        System.out.println("有人离开~");
        sessionMap.remove(username);
    }
    @OnError
    public void onError(Session session, Throwable error) {
        System.out.println("发生错误");
        error.printStackTrace();
    }

}
