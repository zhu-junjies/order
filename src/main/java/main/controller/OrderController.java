package main.controller;

import main.entity.Order;
import main.service.OrderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (Order)表控制层
 *
 * @author makejava
 * @since 2021-06-05 09:53:30
 */
@RestController
@RequestMapping("order")
public class OrderController {
    /**
     * 服务对象
     */
    @Resource
    private OrderService orderService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @RequestMapping("/updateStatus")
    public void updateStatus(@RequestParam("orderId") String orderId){
        orderService.updateStatusByOrderid(orderId);
    }
}
