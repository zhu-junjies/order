package main.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import main.entity.Order;
import main.entity.Orderitem;
import main.entity.addOrder;
import main.service.DishitemService;
import main.service.OrderService;
import main.service.OrderitemService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static net.sf.json.JSONArray.getCollectionType;

/**
 * (Orderitem)表控制层
 *
 * @author makejava
 * @since 2021-06-05 09:53:20
 */
@RestController
@RequestMapping("orderitem")
public class OrderitemController {
    /**
     * 服务对象
     */
    @Resource
    private OrderitemService orderitemService;

    @Resource
    private DishitemService dishitemService;

    @Resource
    private OrderService orderService;

    @RequestMapping("/addOrder")
    public String addOrder(@CookieValue("username")String username,@RequestParam("orderitems") String orderitemss, String dishId) throws JsonProcessingException {
        final ObjectMapper mapper = new ObjectMapper();
        List<Orderitem> orderitems = mapper.readValue(orderitemss, new TypeReference<List<Orderitem>>(){});
//        Orderitem[] orderitems;
        Date date = new Date();
        Long orderId= date.getTime();
        for (Orderitem orderitem : orderitems) {
            Integer price = Integer.valueOf(dishitemService.queryByDishName(orderitem.getDishName()).getPrice());
           orderitem.setOrderid(orderId.toString());
            orderitem.setPrice(price*orderitem.getNum());
            orderitemService.insert(orderitem);
        }
//
        Order order = new Order();
        order.setConsumer(username);
        order.setDishid(dishId);
        order.setOrderitems(orderitems);
        order.setOrderid(orderId.toString());
        order.setStatus("-1");
        orderService.insert(order);
        return "ok";
    }
    @RequestMapping("/getOrderByConsumer")
    public List<Order> queryOrderByConsumer(@CookieValue("username") String username)
    {
        DecimalFormat df   = new DecimalFormat("######0.00");
        List<Order> orders = orderService.queryByConsumer(username);
        for (Order order : orders) {
            List<Orderitem> orderitems = order.getOrderitems();
            order.setTotalPrice(0);
            for (Orderitem orderitem : orderitems) {
                double price = orderitem.getPrice()*orderitem.getNum();
                order.setTotalPrice(order.getTotalPrice()+price);
            }
        }
        System.out.println(orders);
        return orders;
    }
    @RequestMapping("/getBusidByConsumer")
    public List<String> queryBusidByConsumer(@CookieValue("username") String username)
    {
        return orderService.queryBusidByConsumer(username);
    }
    @RequestMapping("/getOrderByBusId")
    public List<Order> queryOrderByBusId(@CookieValue("username") String username)
    {
        DecimalFormat df   = new DecimalFormat("######0.00");
        List<Order> orders = orderService.queryByBusId(username);
        for (Order order : orders) {
            List<Orderitem> orderitems = order.getOrderitems();
            order.setTotalPrice(0);
            for (Orderitem orderitem : orderitems) {
                double price = orderitem.getPrice()*orderitem.getNum();
                order.setTotalPrice(order.getTotalPrice()+price);
            }
        }
        System.out.println(orders);
        return orders;
    }
}
