package main.controller;

import main.entity.Bus;
import main.entity.IndexData;
import main.service.BusService;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * (Bus)表控制层
 *
 * @author makejava
 * @since 2021-06-05 13:57:56
 */
@RestController
@RequestMapping("bus")
public class BusController {
    /**
     * 服务对象
     */
    @Resource
    private BusService busService;


    @PostMapping("/register")
    public String register(Bus bus, HttpServletRequest request, @RequestPart("file")MultipartFile multipartFile) throws FileNotFoundException {
        HttpSession session = request.getSession();
        Date date = new Date();
        String originalFilename = multipartFile.getOriginalFilename();
        originalFilename = originalFilename.substring(originalFilename.lastIndexOf("."));
        File paths= new File(ResourceUtils.getURL("classpath:").getPath());
        System.out.println(ResourceUtils.getURL("classpath:").getPath());
        String path="C:\\test\\static\\image\\"+date.getTime()+(int)Math.random()*100+originalFilename;
        System.out.println(busService.queryByBusinessName(bus.getBusinessname()));
        System.out.println(busService.queryOneByUsername(bus.getUsername()));
        if (busService.queryByBusinessName(bus.getBusinessname())!=null||busService.queryOneByUsername(bus.getUsername())!=null)
        {
            return "no";
        }
        try {
            multipartFile.transferTo(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        path="http://localhost:8089/res/"+path.substring(path.lastIndexOf("\\"));
        System.out.println(path);
        bus.setFilepath(path);
        busService.insert(bus);
        return "ok";
    }
    @RequestMapping("/querypage")
    public IndexData queryByPage(@RequestParam("page")Integer page)
    {
        IndexData indexData = new IndexData();
        List<Bus> buses = busService.queryAllByLimit((page - 1) * 4, 4);
        System.out.println(buses);
        indexData.setMainData(buses);
        indexData.setTotal(busService.getBusCount());
        return indexData;
    }
    @RequestMapping("/queryBySelf")
    public Bus queryBySelf(@CookieValue("username")String username)
    {
        Bus bus = busService.queryOneByUsername(username);
        return bus;
    }
    @RequestMapping("/updatePhoto")
    public void updatePhoto(@RequestPart("photo")MultipartFile photo,@RequestParam("file")String path) throws IOException {
        String substring = path.substring(path.lastIndexOf("/"));
        String paths="C:\\test\\static\\image\\"+substring;
        photo.transferTo(new File(paths));
    }
    @RequestMapping("/updateBus")
    public void  updateBus(Bus bus){
         busService.update(bus);
    }
}
