package main.controller;

import main.entity.Message;
import main.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RequestMapping("messagea")
@RestController()
public class MessageApi {

    @Autowired
    MessageService messageService;

    @RequestMapping("/getMessage")
    public List<Message> getMessage(@CookieValue("username")String username, @RequestParam("busId")String busId)
    {
        List<Message> messages = messageService.queryByUsername(username, busId);
        return messages;
    }
}
