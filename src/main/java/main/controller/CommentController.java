package main.controller;

import main.entity.Comment;
import main.service.CommentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

/**
 * (Comment)表控制层
 *
 * @author makejava
 * @since 2021-06-07 10:10:23
 */
@RestController
@RequestMapping("comment")
public class CommentController {
    /**
     * 服务对象
     */
    @Resource
    private CommentService commentService;
    @RequestMapping("/getComments")
    public List<Comment> getCommets(String busid,int start,int size)
    {
       return commentService.queryByBusid(busid,start,size);
    }
    @RequestMapping("/getCount")
    public int getCount(String busid){
        return commentService.queryCount(busid);
    }
    @RequestMapping("/getAvgRate")
    public String getAvgRate(String busid){
        DecimalFormat    df   = new DecimalFormat("######0.0");
        return busid==null? "0":df.format(commentService.queryAvgRate(busid));
    }
    @RequestMapping("/addComment")
    public String addComment(Comment comment, @CookieValue("username") String username){
        System.out.println(username);
        Date date = new Date();
        Timestamp timestamp= new Timestamp(date.getTime());
        comment.setDate(timestamp.toString());
        comment.setUsername((String) username);
        Comment comment1 = commentService.queryByBusIdAndUsername(comment.getBusid(), comment.getUsername());
        if (comment1==null) {
            commentService.insert(comment);
            return "ok";
        }
        commentService.update(comment);
        return "no";
    }
    @RequestMapping("/updateReply")
    public void updateReply(String username,String replymessage,@CookieValue("username")String busid)
    {
        commentService.updateReply(busid, replymessage,username);
    }
}
