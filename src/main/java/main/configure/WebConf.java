package main.configure;

import main.Converter.IntegerConverter;
import main.Intercepter.CorsItercepter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

@Configuration
public class WebConf implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new CorsItercepter()).addPathPatterns("/**") .excludePathPatterns("/","/res/**","/login","/res/css/**","/fonts/**","/res/image/**","/res/script/**","/res/static/fonts/**");

    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new IntegerConverter());
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/res/**").addResourceLocations("file:C:\\test\\static\\image\\");

    }
}
