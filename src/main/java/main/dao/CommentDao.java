package main.dao;

import main.entity.Comment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Comment)表数据库访问层
 *
 * @author makejava
 * @since 2021-06-07 10:10:23
 */
@Mapper
public interface CommentDao {


    int queryCount(String Busid);

    List<Comment> queryByBusid(@Param("Busid") String Busid,@Param("page") int pageStart,@Param("limit") int limit);



    List<Comment> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    Double queryAvgRate(String BusId);

    Comment queryByBusIdAndUsername(String username,String busId);

    /**
     * 新增数据
     *
     * @param comment 实例对象
     * @return 影响行数
     */
    int insert(Comment comment);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Comment> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Comment> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Comment> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<Comment> entities);

    /**
     *
     * @param busId
     * @param replyMessage
     */
    void updateReply(@Param("busId") String busId,@Param("replyMessage") String replyMessage,@Param("username")String username);


    void update(Comment comment);

}

