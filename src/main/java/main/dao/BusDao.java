package main.dao;

import main.entity.Bus;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Bus)表数据库访问层
 *
 * @author makejava
 * @since 2021-06-05 13:57:55
 */
@Mapper
public interface BusDao<queryOneByUsername> {

    /**
     * 通过ID查询单条数据
     *
     * @param businessname 主键
     * @return 实例对象
     */
    Bus queryById(String businessname);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Bus> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param bus 实例对象
     * @return 对象列表
     */
    List<Bus> queryAll(Bus bus);

    /**
     * 新增数据
     *
     * @param bus 实例对象
     * @return 影响行数
     */
    int insert(Bus bus);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Bus> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Bus> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Bus> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<Bus> entities);

    /**
     * 修改数据
     *
     * @param bus 实例对象
     * @return 影响行数
     */
    int update(Bus bus);

    /**
     * 通过主键删除数据
     *
     * @param businessname 主键
     * @return 影响行数
     */
    int deleteById(String businessname);


    Integer queryCount();

    Bus queryOneByUsername(String username);

}

