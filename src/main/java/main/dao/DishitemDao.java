package main.dao;

import main.entity.Dishitem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * (Dishitem)表数据库访问层
 *
 * @author makejava
 * @since 2021-06-06 19:55:54
 */
@Mapper
public interface DishitemDao {

    /**
     * 通过ID查询单条数据
     *
     * @param busId 主键
     * @return 实例对象
     */
    List<Dishitem> queryByBusId(String busId);



    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Dishitem> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param dishitem 实例对象
     * @return 对象列表
     */
    List<Dishitem> queryAll(Dishitem dishitem);

    /**
     * 新增数据
     *
     * @param dishitem 实例对象
     * @return 影响行数
     */
    int insert(Dishitem dishitem);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Dishitem> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Dishitem> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Dishitem> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<Dishitem> entities);

    /**
     * 修改数据
     *
     * @param dishitem 实例对象
     * @return 影响行数
     */
    int update(Dishitem dishitem);

    /**
     * 通过主键删除数据
     *
     * @param dishname 主键
     * @return 影响行数
     */
    int deleteById(String dishname);
    /**
     *
     */

    Dishitem queryByDishName(String dishName);


    Dishitem queryByDishNameAndBusId(@Param("dishName") String dishName,@Param("busId") String busId);



}

