package main.dao;

import main.entity.Gender;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Gender)表数据库访问层
 *
 * @author makejava
 * @since 2021-06-08 14:24:54
 */
@Mapper
public interface GenderDao {

    /**
     * 通过ID查询单条数据
     *
     * @param username 主键
     * @return 实例对象
     */
    Gender queryById(String username);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Gender> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param gender 实例对象
     * @return 对象列表
     */
    List<Gender> queryAll(Gender gender);

    /**
     * 新增数据
     *
     * @param gender 实例对象
     * @return 影响行数
     */
    int insert(Gender gender);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Gender> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Gender> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Gender> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<Gender> entities);

    /**
     * 修改数据
     *
     * @param gender 实例对象
     * @return 影响行数
     */
    int update(Gender gender);

    /**
     * 通过主键删除数据
     *
     * @param username 主键
     * @return 影响行数
     */
    int deleteById(String username);

}

