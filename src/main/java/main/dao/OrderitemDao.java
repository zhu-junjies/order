package main.dao;

import main.entity.Orderitem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Orderitem)表数据库访问层
 *
 * @author makejava
 * @since 2021-06-05 09:53:19
 */
@Mapper
public interface OrderitemDao {

    /**
     * 通过ID查询单条数据
     *
     * @param orderId 主键
     * @return 实例对象
     */
    Orderitem queryByOrderId(String orderId);


    /**
     *
     * @param dishname
     * @return ss
     */
    Orderitem queryById(String dishname);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Orderitem> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param orderitem 实例对象
     * @return 对象列表
     */
    List<Orderitem> queryAll(Orderitem orderitem);

    /**
     * 新增数据
     *
     * @param orderitem 实例对象
     * @return 影响行数
     */
    int insert(Orderitem orderitem);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Orderitem> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Orderitem> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Orderitem> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<Orderitem> entities);

    /**
     * 修改数据
     *
     * @param orderitem 实例对象
     * @return 影响行数
     */
    int update(Orderitem orderitem);

    /**
     * 通过主键删除数据
     *
     * @param dishname 主键
     * @return 影响行数
     */
    int deleteById(String dishname);

    List<String>queryBusidByConsumer(String consumer);

}

