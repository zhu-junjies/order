package main.Converter;

import org.springframework.core.convert.converter.Converter;

/**
 * @author win10
 */
public class IntegerConverter implements Converter<String,Integer> {

    @Override
    public Integer convert(String s) {
        if (s.equals("男")){
            return 0;
        }
        else if (s.equals("女")){
            return 1;
        }
        else {
            return Integer.valueOf(s);
        }
    }
}
