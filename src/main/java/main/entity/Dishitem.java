package main.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.io.Serializable;

/**
 * (Dishitem)实体类
 *
 * @author makejava
 * @since 2021-06-06 19:55:51
 */
public class Dishitem implements Serializable {
    private static final long serialVersionUID = 750886696222060424L;


    @JsonProperty("dishName")
    private String dishname;

    private String price;

    private String dishid;

    private String busid;

    private String num="0";


    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    private boolean detailVisible=false;

    public boolean isDetailVisible() {
        return detailVisible;
    }

    public void setDetailVisible(boolean detailVisible) {
        this.detailVisible = detailVisible;
    }

    public String getDishname() {
        return dishname;
    }

    public void setDishname(String dishname) {
        this.dishname = dishname;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDishid() {
        return dishid;
    }

    public void setDishid(String dishid) {
        this.dishid = dishid;
    }

    public String getBusid() {
        return busid;
    }

    public void setBusid(String busid) {
        this.busid = busid;
    }

    @Override
    public String toString() {
        return "Dishitem{" +
                "dishname='" + dishname + '\'' +
                ", price='" + price + '\'' +
                ", dishid='" + dishid + '\'' +
                ", busid='" + busid + '\'' +
                ", num='" + num + '\'' +
                ", detailVisible=" + detailVisible +
                '}';
    }
}
