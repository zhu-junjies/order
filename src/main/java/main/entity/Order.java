package main.entity;

import java.io.Serializable;
import java.util.List;

/**
 * (Order)实体类
 *
 * @author makejava
 * @since 2021-06-05 09:53:30
 */
public class Order implements Serializable {
    private static final long serialVersionUID = 181882562848454157L;

    private String orderid;

    private String dishid;

    private String consumer;

    private List<Orderitem> orderitems;

    private String status;

    private boolean detailVisible=false;

    private double totalPrice;

    @Override
    public String toString() {
        return "Order{" +
                "orderid='" + orderid + '\'' +
                ", dishid='" + dishid + '\'' +
                ", consumer='" + consumer + '\'' +
                ", orderitems=" + orderitems +
                ", status='" + status + '\'' +
                ", detailVisible=" + detailVisible +
                ", totalPrice=" + totalPrice +
                '}';
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public boolean isDetailVisible() {
        return detailVisible;
    }

    public void setDetailVisible(boolean detailVisible) {
        this.detailVisible = detailVisible;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Orderitem> getOrderitems() {
        return orderitems;
    }

    public void setOrderitems(List<Orderitem> orderitems) {
        this.orderitems = orderitems;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getDishid() {
        return dishid;
    }

    public void setDishid(String dishid) {
        this.dishid = dishid;
    }

    public String getConsumer() {
        return consumer;
    }

    public void setConsumer(String consumer) {
        this.consumer = consumer;
    }

}
