package main.entity;

import java.util.List;

/**
 * @author win10
 */
public class IndexData {
    private List<Bus> mainData;
    private int total;

    public List<Bus> getMainData() {
        return mainData;
    }

    public void setMainData(List<Bus> mainData) {
        this.mainData = mainData;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "IndexData{" +
                "mainData=" + mainData +
                ", total=" + total +
                '}';
    }
}
