package main.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

/**
 * (Gender)实体类
 *
 * @author makejava
 * @since 2021-06-08 14:24:49
 */
public class Gender implements Serializable {
    private static final long serialVersionUID = 802518113817296728L;

    private Integer gender;

    private String username;

    private String date;

    private Double money;

    private String name;

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Gender{" +
                "gender=" + gender +
                ", username='" + username + '\'' +
                ", date='" + date + '\'' +
                ", money=" + money +
                ", name='" + name + '\'' +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

}
