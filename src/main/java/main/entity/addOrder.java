package main.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class addOrder {
    private ArrayList<Orderitem> orderitems;
    private String dishId;

    @Override
    public String toString() {
        return "addOrder{" +
                "orderitems=" + orderitems +
                ", dishId='" + dishId + '\'' +
                '}';
    }

    public List<Orderitem> getOrderitems() {
        return orderitems;
    }

    public void setOrderitems(List<Orderitem> orderitems) {
        this.orderitems = (ArrayList<Orderitem>) orderitems;
    }

    public String getDishId() {
        return dishId;
    }

    public void setDishId(String dishId) {
        this.dishId = dishId;
    }
}
