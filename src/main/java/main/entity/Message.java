package main.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * (Message)实体类
 *
 * @author makejava
 * @since 2021-06-09 08:33:13
 */
public class Message implements Serializable {
    private static final long serialVersionUID = 333722765630724815L;

    private String username;

    private String tousername;

    private String content;

    private String time;

    @Override
    public String toString() {
        return "Message{" +
                "username='" + username + '\'' +
                ", tousername='" + tousername + '\'' +
                ", content='" + content + '\'' +
                ", time=" + time +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTousername() {
        return tousername;
    }

    public void setTousername(String tousername) {
        this.tousername = tousername;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
