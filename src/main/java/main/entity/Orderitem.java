package main.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * (Orderitem)实体类
 *
 * @author makejava
 * @since 2021-06-05 09:53:19
 */
public class Orderitem implements Serializable {
    private static final long serialVersionUID = -35237880640632357L;


    private String dishName;

    private Integer num;

    private String busid;

    @JsonIgnore
    private String orderid;

    private double price;

    private String dishid;

    private String detailVisible;

    public String getBusid() {
        return busid;
    }

    public void setBusid(String busid) {
        this.busid = busid;
    }

    public String getDetailVisible() {
        return detailVisible;
    }

    public void setDetailVisible(String detailVisible) {
        this.detailVisible = detailVisible;
    }

    public String getDishid() {
        return dishid;
    }

    public void setDishid(String dishid) {
        this.dishid = dishid;
    }

    @Override
    public String toString() {
        return "Orderitem{" +
                "dishName='" + dishName + '\'' +
                ", num=" + num +
                ", orderid='" + orderid + '\'' +
                ", price=" + price +
                ", dishid='" + dishid + '\'' +
                '}';
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

}
