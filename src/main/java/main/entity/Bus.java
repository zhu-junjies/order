package main.entity;

import java.io.Serializable;

/**
 * (Bus)实体类
 *
 * @author makejava
 * @since 2021-06-05 13:57:53
 */
public class Bus implements Serializable {
    private static final long serialVersionUID = 630003121523813652L;

    private String businessname;

    private String description;

    private String filepath;

    private String username;


    public String getBusinessname() {
        return businessname;
    }

    public void setBusinessname(String businessname) {
        this.businessname = businessname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "Bus{" +
                "businessname='" + businessname + '\'' +
                ", description='" + description + '\'' +
                ", filepath='" + filepath + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
