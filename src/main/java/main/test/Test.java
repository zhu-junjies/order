package main.test;

import java.sql.Timestamp;
import java.util.Date;

public class Test {
    @org.junit.Test
    public void test()
    {
        Date date = new Date();
        System.out.println(date);
        java.sql.Date date1 = new java.sql.Date(date.getTime());
        System.out.println(date1);

        long time = date.getTime();
        Timestamp timestamp = new Timestamp(time);
        System.out.println(timestamp);
    }
}
